package com.example.demo.service;

import com.example.demo.model.Car;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
public class CarService {

    public CompletableFuture<Void> save(List<Car> cars) {
        return CompletableFuture.supplyAsync(() -> saveAll(cars))
                .thenRunAsync(() -> publishAll(cars));
    }

    private CompletableFuture<Void> saveAll(List<Car> cars) {
        return CompletableFuture.runAsync(() -> {
            for (Car car : cars) {
                System.out.println(car);
                saveItem(car);
            }
        });
    }

    private void saveItem(Car car) {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private CompletableFuture<Void> publishAll(List<Car> cars) {
        return CompletableFuture.runAsync(() -> {
            System.out.println(cars);
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
    }


}
