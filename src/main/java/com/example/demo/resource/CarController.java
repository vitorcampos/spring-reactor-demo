package com.example.demo.resource;

import com.example.demo.model.Car;
import com.example.demo.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import static org.springframework.http.ResponseEntity.ok;

@RestController
public class CarController {

    @Autowired
    private CarService carService;

    @PostMapping("/cars")
    public CompletableFuture<ResponseEntity> createCars(@RequestBody List<Car> cars) {
        return carService.save(cars).thenApply(status -> ok().build());
    }
}
