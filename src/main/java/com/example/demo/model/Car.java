package com.example.demo.model;

import java.io.Serializable;
import java.util.Objects;

public class Car implements Serializable {

    private String brand;
    private String model;
    private String fabricationYear;
    private String modelYear;

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getFabricationYear() {
        return fabricationYear;
    }

    public void setFabricationYear(String fabricationYear) {
        this.fabricationYear = fabricationYear;
    }

    public String getModelYear() {
        return modelYear;
    }

    public void setModelYear(String modelYear) {
        this.modelYear = modelYear;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return Objects.equals(brand, car.brand) &&
                Objects.equals(model, car.model) &&
                Objects.equals(fabricationYear, car.fabricationYear) &&
                Objects.equals(modelYear, car.modelYear);
    }

    @Override
    public int hashCode() {

        return Objects.hash(brand, model, fabricationYear, modelYear);
    }

    @Override
    public String toString() {
        return "Car{" +
                "brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", fabricationYear='" + fabricationYear + '\'' +
                ", modelYear='" + modelYear + '\'' +
                '}';
    }
}
